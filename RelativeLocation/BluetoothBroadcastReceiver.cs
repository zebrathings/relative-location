﻿using Android.Bluetooth;
using Android.Content;
using Java.Lang;
using System;

namespace RelativeLocation.Droid
{
    public class BluetoothBroadcastReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            var receiveTime = DateTime.UtcNow;
            if (App.ScanResults != null && intent != null && BluetoothDevice.ActionFound == intent.Action)
            {
                App.ScanResults.Add(new BSSIDInfo
                {
                    BSSID = ((BluetoothDevice)intent.GetParcelableExtra(BluetoothDevice.ExtraDevice))?.Address?.ToUpper(),
                    SignalStrength = intent.GetShortExtra(BluetoothDevice.ExtraRssi, Short.MinValue),
                    SignalType = SignalType.Bluetooth,
                    RecUtc = receiveTime,
                });
            }
        }
    }
}