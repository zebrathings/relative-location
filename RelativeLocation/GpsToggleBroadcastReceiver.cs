﻿using Android.Content;
using Android.Locations;

namespace RelativeLocation.Droid
{
    public class GpsToggleBroadcastReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            if (intent.Action == LocationManager.ProvidersChangedAction)
            {
                App.EnableGeolocation();
            }
        }
    }
}