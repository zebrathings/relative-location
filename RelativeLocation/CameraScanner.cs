﻿using iFactr.Core;
using iFactr.Core.Layers;
using MonoCross.Scanning;
using System;
using System.Collections.Generic;
using ZXing.Mobile;

namespace iFactr.Droid
{
    public class CameraScanner : IScanner
    {
        private MobileBarcodeScanner _scanner;
        public event ScanEventDelegate ScanOccurred;

        public bool InitScanner()
        {
            MobileBarcodeScanner.Initialize(DroidFactory.MainActivity.Application);
            _scanner = new MobileBarcodeScanner();
            return true;
        }

        public void NullifyScanOccurredEvent()
        {
            ScanOccurred = null;
        }

        public void RaiseScanOccurred(string code)
        {
            ScanOccurred?.Invoke(code);
        }

        public void StartScan()
        {
            StartScan(false);
        }

        public async void StartScan(bool triggerSoftAlways)
        {
            if (!triggerSoftAlways)
            {
                return;
            }

            if (_scanner == null)
            {
                InitScanner();
            }

            var result = await _scanner.Scan();
            if (result == null)
            {
                // Scan cancelled.
                return;
            }

            iApp.Session.TryGetValue("MultiScan", out object multi);
            var results = new List<string>();
            do
            {
                results.Add($"{result.Text};{result.BarcodeFormat};{DateTime.Now}");
            } while (Convert.ToBoolean(multi) && (result = await _scanner.Scan()) != null);

            var resultString = string.Join(ScanLayer.BarcodeSeparatorChar, results);
            RaiseScanOccurred(resultString);
        }

        public void StopScan()
        {
            NullifyScanOccurredEvent();
            _scanner?.Cancel();
        }

        public void StopScan(ScanEventDelegate scanDelegate)
        {
            ScanOccurred -= scanDelegate;
            StopScan();
        }

        public void TermScanner()
        {
            _scanner = null;
        }
    }
}