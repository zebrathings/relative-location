﻿using Android.App;
using MonoCross.Navigation;
using MonoCross.Scanning;
using MonoCross.Utilities;
using Symbol.XamarinEMDK;
using Symbol.XamarinEMDK.Barcode;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using static Symbol.XamarinEMDK.EMDKManager;

namespace iFactr.Droid
{
    public class ZScanner : Java.Lang.Object, IScanner, IEMDKListener
    {
        #region Singleton

        public ZScanner()
        {
            SetEMDKContextListener();
        }

        ~ZScanner()
        {
            TermScanner();
            ReleaseBarcodeManager();
            ReleaseEMDKManager();
        }

        public static ZScanner Instance
        {
            get { return MXContainer.Resolve<IScanner>() as ZScanner; }
            set { MXContainer.RegisterSingleton<IScanner>(value); }
        }

        #endregion

        private Symbol.XamarinEMDK.Barcode.Scanner _scanner;
        private IList<ScannerInfo> _deviceList;
        private BarcodeManager _barcodeManager;
        private EMDKManager _emdkManager;
        private int _scannerIndex = 0;

        private bool isContinuousMode = false;

        private Stopwatch scannerTimer = new Stopwatch();

        public const int ScannerTimerMinTime = 333;

        public bool UseLabel { get; set; }

        public bool IsReading { get; set; }

        public int ScanCount { get; set; }

        public int ScannerTriggerType { get; set; }

        /// <summary>
        /// Minimum time in milliseconds between hardware scans on android.  
        /// The minimum value is 333 ms, any less will be ignored.
        /// </summary>
        public int ScannerPause { get; set; }

        /// <summary>
        /// Tell the scanner to enable continuous mode (when idle, read again after 100 ms or so)
        /// </summary>
        public bool ContinuousScanning { get; set; } = true;

        /// <summary>
        /// Set to true to have the ZScanner on Android to signal the GC to collect
        /// </summary>
        public bool ScannerGCCollect { get; set; }

        #region IEMDKListener implementation

        public void OnOpened(EMDKManager emdkManager)
        {
            try
            {
#if EMDK_FAIL_01
                Device.Log.Debug("EMDK Failed to open!");
                return;
#endif
                Device.Log.Debug("EMDK Opened Success!");
                _emdkManager = emdkManager;
                // Acquire the barcode manager resources
                _barcodeManager = (BarcodeManager)_emdkManager.GetInstance(FEATURE_TYPE.Barcode);

                if (_barcodeManager != null)
                {
                    // Add connection listener
                    _barcodeManager.Connection -= barcodeManager_Connection;
                    _barcodeManager.Connection += barcodeManager_Connection;
                }
            }
            catch (Exception ex)
            {
                Device.Log.Error($"OnOpened Error!");
                Device.Log.Error(ex);
            }
        }

        public void OnClosed()
        {
            // This callback will be issued when the EMDK closes unexpectedly.
            try
            {
                Device.Log.Error("Status: EMDK unexpected OnClosed, attempting to reacquire as a listener.");

                // release and disable the scanner.
                TermScanner();

                // release our barcode manager
                ReleaseBarcodeManager();

                // Release the EMDK
                ReleaseEMDKManager();

                // reinitialize our grasp on the EMDK hopefully..
                SetEMDKContextListener();
            }
            catch (Exception ex)
            {
                Device.Log.Error(ex);
            }
        }

        #endregion

        #region IScanner implementation

        public void SetEMDKContextListener()
        {
            if (_emdkManager != null)
            {
                Device.Log.Debug("EMDKManager already set.");
                return;
            }
            Device.Log.Debug("Applying for EMDKManager.");

            var results = GetEMDKManager(Application.Context, this);
            if (results.StatusCode == EMDKResults.STATUS_CODE.Success)
            {
                Device.Log.Debug("Status: EMDKManager object request success.");
            }
            else
            {
                Device.Log.Error("Status: EMDKManager object request failed!");
            }
        }

        public event ScanEventDelegate ScanOccurred;

        public virtual bool InitScanner()
        {
            bool flag = true;
            try
            {
                if (_scanner != null)
                {
                    Device.Log.Debug($"{nameof(ZScanner)} >> InitScanner but scanner is already set.");
                }
                else
                {
                    _deviceList = _barcodeManager.SupportedDevicesInfo;
                    if (_deviceList?.Count > 0)
                    {
                        // Get new scanner device based on the selected index
                        Device.Log.Info("-- Scanners found! -- ");

                        for (int i = 0; i < _deviceList.Count; i++)
                        {
                            Device.Log.Info($"Scanner: [{_deviceList[i].FriendlyName}]:{_deviceList[i].DeviceIdentifier}\r\nConnection Type:{_deviceList[i].GetConnectionType()}\r\nDefault Scanner:{_deviceList[i].IsDefaultScanner}");

                            if (_deviceList[i].IsDefaultScanner)
                            {
                                _scannerIndex = i;
                            }
                        }
#if EMDK_FAIL_02
                        scanner = null;
#else
                        _scanner = _barcodeManager.GetDevice(_deviceList[_scannerIndex]);
#endif
                    }
                    else
                    {
                        flag = false;
                        Device.Log.Error($"{nameof(ZScanner)} >> Status: Failed to get the specified scanner device! Please close and restart the application.");
                        return flag;
                    }

                    if (_scanner != null)
                    {
                        // Add data listener
                        _scanner.Data += scanner_Data;

                        // Add status listener
                        _scanner.Status += scanner_Status;

                        SetTrigger();

                        try
                        {
                            // Enable the scanner
                            _scanner.Enable();
                        }
                        catch (ScannerException e)
                        {
                            flag = false;
                            Device.Log.Error(e);
                        }
                    }
                    else
                    {
                        flag = false;
                        Device.Log.Error($"EMDK_FAIL_02: Failed to find scanning device during initialization.");
                    }
                }
            }
            catch (NullReferenceException nre)
            {
                Device.Log.Error($"EMDK_FAIL_01: Could not acquire barcode scanner on device.  EMDK_FAIL_01", nre);
            }
            catch (Exception ex)
            {
                Device.Log.Error($"Scanner Failed: Failed to initialize the scanner", ex);
            }

            return flag;
        }

        public void NullifyScanOccurredEvent()
        {
            ScanOccurred = null;
        }

        void IScanner.TermScanner()
        {
            Device.Log.Debug($"{nameof(ZScanner)} >> {nameof(IScanner.TermScanner)} called.");
            TermScanner();
        }

        public void StartScan()
        {
            try
            {
                isContinuousMode = ContinuousScanning;

                if (_scanner == null)
                {
                    InitScanner();
                }

                if (_scanner != null)
                {
                    if (_scanner.IsEnabled)
                    {
                        BlockScanReadForMinTimer();
                    }
                    else
                    {
                        Device.Log.Debug($"{nameof(ZScanner)} >> Status: Scanner is not enabled");
                    }
                }
            }
            catch (ScannerException ex)
            {
                Device.Log.Error(ex);
            }
        }

        private Random _rand = new Random();

        public void BlockScanReadForMinTimer()
        {
            if (scannerTimer.IsRunning)
            {
                var elapsedTime = scannerTimer.ElapsedMilliseconds;
                var delay = Math.Max(ScannerTimerMinTime, ScannerPause);
                while (elapsedTime < delay)
                {
                    Device.Log.Debug($"{nameof(ZScanner)} >> Read before min time alotted.");
                    Device.Log.Info($"Thread:{Thread.CurrentThread.Name}, id:{Thread.CurrentThread.ManagedThreadId}, bg:{Thread.CurrentThread.IsBackground} pool:{Thread.CurrentThread.IsThreadPoolThread}");
                    var rng = _rand.Next() % ScannerTimerMinTime;
                    Thread.Sleep((int)Math.Max(0, delay - elapsedTime + rng));
                    elapsedTime = scannerTimer.ElapsedMilliseconds;
                }
            }

            try
            {
                if (_scanner == null)
                {
                    Device.Log.Debug($"{nameof(ZScanner)} >> An attempt to scan is being stopped because the scanner is null!");
                    return;
                }
                _scanner.Read();
            }
            catch (ScannerException se)
            {
                if (se.Result == ScannerResults.AlreadyScanning)
                {
                    Device.Log.Debug($"{nameof(ZScanner)} >> Scanner Already Scanning, there was no reason to be here.");
                    Device.Thread.QueueWorker((o) =>
                    {
                        new ManualResetEventSlim().Wait(25);
                        if (!isContinuousMode)
                        {
                            StartScan();
                        }
                    });
                }
                else if (se.Result == ScannerResults.Failure)
                {
                    Device.Log.Debug($"{nameof(ZScanner)} >> Scanner Failure -- It's likely you are here because you tried to activate the scanner before it was ready.");
                }
                else if (se.Result == ScannerResults.ScannerNotConnected)
                {
                    Device.Log.Error("Scanner not connected == Error Below == Attempting to reconnect.");
                    Device.Log.Error(se);
                    TermScanner();
                    InitScanner();
                }
                else Device.Log.Error(se);
            }
            finally
            {
                IsReading = true;

                scannerTimer.Reset();
                scannerTimer.Start();
            }
        }

        public virtual void StartScan(bool triggerSoftAlways)
        {
            isContinuousMode = triggerSoftAlways;
            StartScan();
        }

        public virtual void StopScan()
        {
            NullifyScanOccurredEvent();
            if (_scanner != null)
            {
                try
                {
                    // Reset continuous flag 
                    isContinuousMode = false;

                    // Cancel the pending read.
                    _scanner.CancelRead();
                }
                catch (ScannerException ex)
                {
                    if (ex.Result == ScannerResults.Failure)
                    {
                        Device.Log.Debug($"{nameof(ZScanner)} >> Scanner Failure -- Canceling a read, its likely there was no pending read at this time.");
                    }
                    else Device.Log.Error(ex);
                }
            }
        }

        public void StopScan(ScanEventDelegate scanDelegate)
        {
            ScanOccurred -= scanDelegate;
            StopScan();
        }

        public void RaiseScanOccurred(string code)
        {
            IsReading = false;
            ScanCount++;
            Device.Log.Debug($">>SCAN # {ScanCount} OCCURRED: " + code);
            ScanOccurred?.Invoke(code);
            scannerTimer.Reset();
            scannerTimer.Start();

            if (ScannerGCCollect && ScanCount % 100 == 0)
            {
                Device.Log.Debug($"{nameof(ZScanner)} >>> Collecting the GC...");
                GC.Collect();
            }
        }

        #endregion IScanner Implementation

        public virtual void TermScanner()
        {
            try
            {
                Device.Log.Debug($"Deinitializing our scanner (setting it to null).");
                if (_scanner == null)
                {
                    Device.Log.Debug($"Deinitilize scanner failed because it was not initialized (it's already null).");
                }
                if (_scanner != null)
                {
                    try
                    {
                        // Cancel if there is any pending read
                        _scanner.CancelRead();
                    }
                    catch (ScannerException e)
                    {
                        Device.Log.Error(e);
                    }
                    finally
                    {
                        // Disable the scanner 
                        _scanner.Disable();
                    }

                    // Remove data listener
                    _scanner.Data -= scanner_Data;

                    // Remove status listener
                    _scanner.Status -= scanner_Status;

                    try
                    {
                        // Release the scanner
                        _scanner.Release();
                    }
                    catch (ScannerException e)
                    {
                        Device.Log.Error("Error releasing the Scanner");
                        Device.Log.Error(e);
                    }

                    _scanner = null;
                    Device.Log.Debug($"{nameof(ZScanner)}::DeInitScanner scanner set to null");
#if DEBUG
                    Device.Log.Debug(Environment.StackTrace);
#endif
                }
            }
            catch (Exception ex)
            {
                Device.Log.Error(ex);
            }
        }

        private void LogMemoryInfo()
        {
#if MEMORY_WATCH
            ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
            var activityManager = (ActivityManager)DroidFactory.MainActivity.GetSystemService(Android.Content.Context.ActivityService);
            activityManager.GetMemoryInfo(mi);

            double percentFree = mi.AvailMem / (double)mi.TotalMem * 100.0;
            Device.Log.Debug($"MEMINFO : {percentFree.ToString()}");
#endif
        }

        private void barcodeManager_Connection(object sender, BarcodeManager.ScannerConnectionEventArgs e)
        {
            try
            {
                var scannerName = string.Empty;

                var scannerInfo = e.P0;
                var connectionState = e.P1;

                var scannerNameBT = scannerInfo.FriendlyName;

                if (_deviceList.Count != 0)
                {
                    scannerName = _deviceList[_scannerIndex].FriendlyName;
                }

                if (string.Equals(scannerName, scannerNameBT, StringComparison.InvariantCultureIgnoreCase))
                {
                    if (connectionState == BarcodeManager.ConnectionState.Connected)
                    {
                        // Bluetooth scanner connected
                        TermScanner();
                        InitScanner();
                        SetDecoders();
                    }

                    if (connectionState == BarcodeManager.ConnectionState.Disconnected)
                    {
                        // De-initialize scanner
                        TermScanner();
                    }
                }
            }
            catch (Exception ex)
            {
                Device.Log.Error(ex);
            }
        }

        /// <summary>
        /// Called by EMDK calls when barcode is scanned
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="e">Data object containing the scan value, symbology, and local timestamp</param>
        private void scanner_Data(object sender, Symbol.XamarinEMDK.Barcode.Scanner.DataEventArgs e)
        {
            LogMemoryInfo();
            try
            {
                var scanDataCollection = e.P0;
                if (scanDataCollection?.Result == ScannerResults.Success)
                {
                    var scanData = scanDataCollection.GetScanData();
                    foreach (ScanDataCollection.ScanData data in scanData)
                    {
                        string dataString = UseLabel ? $"{data.Data};{data.LabelType};{data.TimeStamp}" : data.Data;
                        RaiseScanOccurred(dataString);

                        Microsoft.AppCenter.Analytics.Analytics.TrackEvent("Scan", new Dictionary<string, string> {
                            {"Data", dataString},
                            {"Scan Count", ScanCount.ToString()}
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Device.Log.Error(ex);
            }
        }

        private void scanner_Status(object sender, Symbol.XamarinEMDK.Barcode.Scanner.StatusEventArgs e)
        {
            LogMemoryInfo();
            var statusData = e.P0;
            var state = statusData.State;
            Device.Log.Debug($">>> Enter scanner_StatusStatus state = {state.ToString()}");

            if (state == Symbol.XamarinEMDK.Barcode.StatusData.ScannerStates.Idle)
            {
                Device.Log.Debug($"Status: {statusData.FriendlyName} is enabled and idle...");

                if (isContinuousMode)
                {
                    try
                    {
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans) 
                        // may cause the scanner to pause momentarily before resuming the scanning. 
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        Thread.Sleep(ScannerPause);

                        // Submit another read to keep the continuation
                        if (_scanner.IsReadPending)
                        {
                            Device.Log.Debug($"{nameof(ZScanner)} Continuous Mode read already pending.");
                        }
                        else
                        {
                            BlockScanReadForMinTimer();
                        }
                    }
                    catch (ScannerException ex)
                    {
                        // Disregard the AlreadyScanning
                        if (ex.Result != ScannerResults.AlreadyScanning)
                        {
                            Device.Log.Error(ex);
                        }
                    }
                    catch (NullReferenceException ex)
                    {
                        Device.Log.Error(ex);
                    }
                }
            }
            else
            {
                Device.Log.Debug(">>Non-idle state = " + state);
            }
        }

        private void SetTrigger()
        {
            if (_scanner == null)
            {
                InitScanner();
            }

            if (_scanner != null)
            {
                switch (ScannerTriggerType)
                {
                    case 0: // Selected "HARD"
                        _scanner.TriggerType = Symbol.XamarinEMDK.Barcode.Scanner.TriggerTypes.Hard;
                        break;
                    case 1: // Selected "SOFT"
                        _scanner.TriggerType = Symbol.XamarinEMDK.Barcode.Scanner.TriggerTypes.SoftAlways;
                        break;
                    default:
                        break;
                }
            }
        }

        private void SetDecoders()
        {
            if (_scanner == null)
            {
                InitScanner();
            }

            if (_scanner != null && _scanner.IsEnabled)
            {
                try
                {
                    // Config object should be taken out before changing.
                    var config = _scanner.GetConfig();

                    // Set EAN8
                    config.DecoderParams.Ean8.Enabled = true;

                    // Set EAN13
                    config.DecoderParams.Ean13.Enabled = true;

                    // Set Code39
                    config.DecoderParams.Code39.Enabled = true;

                    // Set Code128
                    config.DecoderParams.Code128.Enabled = true;

                    // Set DataMatrix
                    config.DecoderParams.DataMatrix.Enabled = true;

                    // Set QRCode
                    config.DecoderParams.QrCode.Enabled = true;

                    // Set UPCA
                    config.DecoderParams.Upca.Enabled = true;

                    // Should be assigned back to the property to get the changes to the lower layers.
                    _scanner.SetConfig(config);
                }
                catch (ScannerException e)
                {
                    Device.Log.Error(e);
                }
            }
        }

        public void OnPause()
        {
            // The application is in background

            // Reset continuous flag 
            isContinuousMode = false;

            // De-initialize scanner
            TermScanner();

            ReleaseBarcodeManager();
        }

        public void ReleaseBarcodeManager()
        {
            if (_barcodeManager != null)
            {
                // Remove connection listener
                _barcodeManager.Connection -= barcodeManager_Connection;
                _barcodeManager = null;

                // Clear scanner list
                _deviceList = null;
            }
        }

        public void ReleaseEMDKManager()
        {
            // Release the barcode manager resources
            if (_emdkManager != null)
            {
                _emdkManager.Release(FEATURE_TYPE.Barcode);
                _emdkManager = null;
            }
        }

        public void OnResume()
        {
            // The application is in foreground
            // Acquire the barcode manager resources
            if (_emdkManager != null)
            {
                try
                {
                    if (_barcodeManager == null)
                    {
                        _barcodeManager = (BarcodeManager)_emdkManager.GetInstance(FEATURE_TYPE.Barcode);

                        if (_barcodeManager != null)
                        {
                            // Add connection listener
                            _barcodeManager.Connection -= barcodeManager_Connection;
                            _barcodeManager.Connection += barcodeManager_Connection;
                            _deviceList = _barcodeManager.SupportedDevicesInfo;
                        }

                        if (IsReading)
                        {
                            StartScan();
                        }
                    }
                }
                catch (Exception e)
                {
                    Device.Log.Error(e);
                    Device.Log.Error("Status: BarcodeManager object creation failed.");
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Device.Log.Debug($"Disposing {nameof(ZScanner)} -> Releasing all resources.");
                OnPause();
                ReleaseEMDKManager();
            }

            base.Dispose(disposing);
        }
    }
}