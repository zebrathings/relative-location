﻿using Android;
using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.Content.PM;
using Android.Locations;
using Android.Net.Wifi;
using Android.OS;
using Android.Runtime;
using Android.Views;
using iFactr.Core;
using iFactr.Core.Forms;
using iFactr.Core.Layers;
using iFactr.Droid;
using iFactr.UI;
using Java.Lang;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using MonoCross.Navigation;
using MonoCross.Utilities.Logging;
using System;
using System.IO;
using System.Linq;
using Alert = iFactr.UI.Alert;

namespace RelativeLocation.Droid
{
    [Activity(MainLauncher = true, WindowSoftInputMode = SoftInput.AdjustPan)]
    public class MainActivity : iFactrActivity
    {
        public const int PermissionRequestCode = 42;

        private readonly BroadcastReceiver _bluetoothReceiver = new BluetoothBroadcastReceiver();
        private readonly BroadcastReceiver _gpsReceiver = new GpsToggleBroadcastReceiver();
        private readonly BluetoothAdapter _adapter = BluetoothAdapter.DefaultAdapter;
        private bool _requestingPermissions;
        private Alert _alert;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            if (DroidFactory.TheApp == null)
            {
                iApp.OnLayerLoadComplete += (iLayer layer) =>
                {
                    DroidFactory.Instance.OutputLayer(layer);
                };
                DroidFactory.AllowReversePortrait = false;
                DroidFactory.MainActivity = this;

                if (!iApp.Settings.ContainsKey(SettingsController.LogFolderKey))
                {
                    App.LogFolder = Path.Combine(Android.OS.Environment.ExternalStorageDirectory.AbsolutePath, PackageName);
                }
                App.DeviceName = Build.Product;
                App.EnableGeolocation = EnableGeolocation;
                App.PerformBSSIDScan = PerformBSSIDScan;
                iApp.Log.OnLogEvent += Log_OnLogEvent;
                AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
                DroidFactory.TheApp = new App();

#if EMDK
                ZScanner.Instance = new ZScanner
                {
                    UseLabel = true,
                };
#else
                MXContainer.RegisterSingleton<MonoCross.Scanning.IScanner>(new CameraScanner());
#endif

                if (Build.VERSION.SdkInt >= BuildVersionCodes.M)
                {
                    _requestingPermissions = true;
                    RequestPermissions(new[] {
                        Manifest.Permission.WriteExternalStorage,
                        Manifest.Permission.AccessFineLocation,
                    }, PermissionRequestCode);
                }
            }
            base.OnCreate(savedInstanceState);

            AppCenter.Start("fcef417d-5d01-4c6b-9eaf-711aca158147", typeof(Analytics), typeof(Crashes));

            RegisterReceiver(_bluetoothReceiver, new IntentFilter(BluetoothDevice.ActionFound));
            RegisterReceiver(_gpsReceiver, new IntentFilter(LocationManager.ProvidersChangedAction));

            if (Build.VERSION.SdkInt < BuildVersionCodes.M)
            {
                StartApp();
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            if (requestCode == PermissionRequestCode && grantResults.All(p => p == Permission.Granted))
            {
                _requestingPermissions = false;
                StartApp();
            }
            else
            {
                var alert = new Alert(DroidFactory.Instance.GetResourceString("PermissionMessage"), DroidFactory.Instance.GetResourceString("PermissionTitle"), AlertButtons.OK);
                alert.Dismissed += (o, e) =>
                {
                    JavaSystem.Exit(0);
                };
                alert.Show();
            }
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        private void StartApp()
        {
            if (_requestingPermissions || _alert != null)
            {
                return;
            }

            var wifiManager = (WifiManager)GetSystemService(WifiService);
            if (!wifiManager.IsWifiEnabled || !_adapter.IsEnabled)
            {
                _alert = new Alert(iApp.Factory.GetResourceString("EnablingRadiosMessage"), iApp.Factory.GetResourceString("EnablingRadiosTitle"), AlertButtons.OK);
                _alert.Dismissed += (o, e) =>
                {
                    wifiManager.SetWifiEnabled(true);
                    _adapter.Enable();
                    _alert = null;
                    EnableGeolocation();
                };
                _alert.Show();
            }
            else
            {
                EnableGeolocation();
            }
        }

        private void EnableGeolocation()
        {
            var useGPS = BoolField.ParseValue(App.GetSetting(SettingsController.GPSKey));
            if (!useGPS || ((LocationManager)GetSystemService(LocationService)).IsProviderEnabled(LocationManager.GpsProvider))
            {
                if (useGPS)
                {
                    App.StartGeolocation();
                }
                if (!PaneManager.Instance.FromNavContext(Pane.Master).Views.Any())
                {
                    iApp.Navigate(DroidFactory.TheApp.NavigateOnLoad);
                }
            }
            else if (_alert == null)
            {
                iApp.Thread.ExecuteOnMainThread(() =>
                {
                    _alert = new Alert(DroidFactory.Instance.GetResourceString("EnableGpsMessage"), DroidFactory.Instance.GetResourceString("EnableGpsTitle"), AlertButtons.OKCancel);
                    _alert.Dismissed += (o, e) =>
                    {
                        if (e.Result == AlertResult.OK)
                        {
                            StartActivity(new Intent(Android.Provider.Settings.ActionLocationSourceSettings));
                            _alert = null;
                        }
                        else
                        {
                            iApp.Settings[SettingsController.GPSKey] = bool.FalseString;
                            iApp.Settings.Store();
                            _alert = null;
                            StartApp();
                        }
                    };
                    _alert.Show();
                });
            }
        }

        public void PerformBSSIDScan(SignalType searchTypes)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            if ((searchTypes & SignalType.Wifi) == SignalType.Wifi)
            {
                var wifiManager = (WifiManager)GetSystemService(WifiService);
                App.ScanResults.AddRange(wifiManager.ScanResults?.Select(r => new BSSIDInfo
                {
                    BSSID = r.Bssid.ToUpper(),
                    SignalStrength = r.Level,
                    SignalType = SignalType.Wifi,
                    RecUtc = epoch.AddMilliseconds(JavaSystem.CurrentTimeMillis() - SystemClock.ElapsedRealtime() + (r.Timestamp / 1000)),
                }) ?? Enumerable.Empty<BSSIDInfo>());
            }

            if ((searchTypes & SignalType.Bluetooth) == SignalType.Bluetooth)
            {
                _adapter.StartDiscovery();
            }
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            iApp.Log.Error((System.Exception)e.ExceptionObject);
        }

        private void Log_OnLogEvent(LogEventArgs logEventArgs)
        {
            switch (logEventArgs.LogLevel)
            {
                case LogMessageType.Error:
                case LogMessageType.Fatal:
                    if (logEventArgs.Exception != null)
                    {
                        Crashes.TrackError(logEventArgs.Exception, new SerializableDictionary<string, string>
                        {
                            { "Message", logEventArgs.Message },
                            { "Level", logEventArgs.LogLevel.ToString() }
                        });
                    }
                    break;
                default:
                    break;
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            UnregisterReceiver(_bluetoothReceiver);
            UnregisterReceiver(_gpsReceiver);
        }

        protected override void OnPause()
        {
            base.OnPause();
#if EMDK
            ZScanner.Instance?.OnPause();
#endif
            _adapter.CancelDiscovery();
        }

        protected override void OnResume()
        {
            base.OnResume();
#if EMDK
            ZScanner.Instance?.OnResume();
#endif
            StartApp();
        }
    }
}