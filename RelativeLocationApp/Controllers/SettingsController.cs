﻿using iFactr.Core;
using iFactr.Core.Forms;
using iFactr.Core.Targets.Settings;
using MonoCross.Navigation;
using MonoCross.Utilities;
using System.Collections.Generic;

namespace RelativeLocation
{
    public class SettingsController : MXController<ISettings>
    {
        public const string APIKey = "APIKey";
        public const string BarcodeURIKey = "BarcodeURI";
        public const string BarcodeInfoURIKey = "BarcodeInfoURI";
        public const string BarcodeTrainingURIKey = "BarcodeTrainingURI";
        public const string BssidURIKey = "BssidURI";
        public const string GPSKey = "GPS";
        public const string ScanIntervalKey = "ScanInterval";
        public const string SignalTypeKey = "SignalType";
        public const string LogDataKey = "LogData";
        public const string LogFolderKey = "LogFolder";
        public const string TrainingModeKey = "TrainingMode";

        public override string Load(string uri, Dictionary<string, string> parameters)
        {
            var perspective = ViewPerspective.Default;
            Model = iApp.Settings;

            if (parameters.Count > 0)
            {
                var gpsKeyUpdateCheck = Model.GetValueOrDefault(GPSKey, string.Empty);
                var apiKeyUpdateCheck = Model.GetValueOrDefault(APIKey, string.Empty);
                Model.AddRange(parameters);
                Model.Store();

                App.LogFolder = App.GetSetting(LogFolderKey);
                var apiKey = Model.GetValueOrDefault(APIKey, string.Empty);
                if (apiKeyUpdateCheck != apiKey)
                {
                    if (string.IsNullOrEmpty(apiKey))
                    {
                        Device.RequestInjectionHeaders.Remove("apikey");
                    }
                    else
                    {
                        Device.RequestInjectionHeaders["apikey"] = apiKey;
                    }
                }
                var gpsKey = Model.GetValueOrDefault(GPSKey, string.Empty);
                if (gpsKeyUpdateCheck != gpsKey)
                {
                    if (BoolField.ParseValue(gpsKey))
                    {
                        App.EnableGeolocation();
                    }
                    else
                    {
                        App.StopGeolocation();
                    }
                }
                perspective = null;
                iApp.Navigate(string.Empty);
            }

            return perspective;
        }
    }
}