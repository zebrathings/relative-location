﻿using MonoCross.Navigation;
using System.Collections.Generic;

namespace RelativeLocation
{
    public class BarcodeController : MXController<BarcodeList>
    {
        private readonly object _modelLock = new object();

        public override string Load(string uri, Dictionary<string, string> parameters)
        {
            var perspective = ViewPerspective.Default;

            lock (_modelLock)
            {
                if (Model == null)
                {
                    Model = new BarcodeList();
                }
                else
                {
                    Model.ScanInterval = int.Parse(App.GetSetting(SettingsController.ScanIntervalKey)) * 1000;
                }
            }

            var description = parameters.GetValueOrDefault("Description");
            if (!string.IsNullOrEmpty(description))
            {
                var info = new BarcodeInfo
                {
                    BarcodeScanUtc = Model.Barcode.BarcodeScanUtc,
                    Symbology = Model.Barcode.Symbology,
                    Value = Model.Barcode.Value,
                    Description = description,
                };

                App.Post(SettingsController.BarcodeInfoURIKey, info, "barcodeinfo");
                Model.Barcode = null;
                perspective = null;
            }

            return perspective;
        }
    }
}