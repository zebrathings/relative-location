﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RelativeLocation.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Strings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Strings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("RelativeLocation.Resources.Strings", typeof(Strings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to API Key.
        /// </summary>
        internal static string APIKey {
            get {
                return ResourceManager.GetString("APIKey", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Relative Location.
        /// </summary>
        internal static string app_name {
            get {
                return ResourceManager.GetString("app_name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Barcode Description Endpoint.
        /// </summary>
        internal static string BarcodeInfoURI {
            get {
                return ResourceManager.GetString("BarcodeInfoURI", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Scan Training Endpoint.
        /// </summary>
        internal static string BarcodeTrainingURI {
            get {
                return ResourceManager.GetString("BarcodeTrainingURI", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Barcode Scan Endpoint.
        /// </summary>
        internal static string BarcodeURI {
            get {
                return ResourceManager.GetString("BarcodeURI", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to BSSID Scan Endpoint.
        /// </summary>
        internal static string BssidURI {
            get {
                return ResourceManager.GetString("BssidURI", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Camera Scan.
        /// </summary>
        internal static string CameraScan {
            get {
                return ResourceManager.GetString("CameraScan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Barcode Description.
        /// </summary>
        internal static string Description {
            get {
                return ResourceManager.GetString("Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please trigger the scanner or use camera scanning below to log a nearby barcode..
        /// </summary>
        internal static string EMDKScanHelp {
            get {
                return ResourceManager.GetString("EMDKScanHelp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please ensure that GPS is enabled on the next screen, or press Cancel to skip geolocation..
        /// </summary>
        internal static string EnableGpsMessage {
            get {
                return ResourceManager.GetString("EnableGpsMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enable GPS.
        /// </summary>
        internal static string EnableGpsTitle {
            get {
                return ResourceManager.GetString("EnableGpsTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to WiFi and Bluetooth will be enabled..
        /// </summary>
        internal static string EnablingRadiosMessage {
            get {
                return ResourceManager.GetString("EnablingRadiosMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enable Signal Scanning.
        /// </summary>
        internal static string EnablingRadiosTitle {
            get {
                return ResourceManager.GetString("EnablingRadiosTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use GPS?.
        /// </summary>
        internal static string GPS {
            get {
                return ResourceManager.GetString("GPS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Log requests and responses?.
        /// </summary>
        internal static string LogData {
            get {
                return ResourceManager.GetString("LogData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Log Folder.
        /// </summary>
        internal static string LogFolder {
            get {
                return ResourceManager.GetString("LogFolder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nearby Barcodes.
        /// </summary>
        internal static string MainTitle {
            get {
                return ResourceManager.GetString("MainTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Permissions denied. Please restart the application and enable permissions..
        /// </summary>
        internal static string PermissionMessage {
            get {
                return ResourceManager.GetString("PermissionMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Permission Denied.
        /// </summary>
        internal static string PermissionTitle {
            get {
                return ResourceManager.GetString("PermissionTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please scan a nearby barcode..
        /// </summary>
        internal static string ScanHelp {
            get {
                return ResourceManager.GetString("ScanHelp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to BSSID Scan Interval (seconds).
        /// </summary>
        internal static string ScanInterval {
            get {
                return ResourceManager.GetString("ScanInterval", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Settings.
        /// </summary>
        internal static string Settings {
            get {
                return ResourceManager.GetString("Settings", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use Bluetooth?.
        /// </summary>
        internal static string SignalType {
            get {
                return ResourceManager.GetString("SignalType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use location training?.
        /// </summary>
        internal static string TrainingMode {
            get {
                return ResourceManager.GetString("TrainingMode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Version.
        /// </summary>
        internal static string VersionLabel {
            get {
                return ResourceManager.GetString("VersionLabel", resourceCulture);
            }
        }
    }
}
