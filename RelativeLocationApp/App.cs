﻿using iFactr.Core;
using iFactr.Core.Forms;
using iFactr.Core.Targets.Settings;
using iFactr.Integrations;
using MonoCross.Navigation;
using MonoCross.Scanning;
using MonoCross.Utilities;
using MonoCross.Utilities.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;

namespace RelativeLocation
{
    public class App : iApp
    {
        private static string _logFolder;
        private static string _defaultLogFolder;
        private GeoLocation _geo;
        private static double _latitude;
        private static double _longitude;

        public static string DeviceName { get; set; }

        public static string LogFolder
        {
            get { return _logFolder; }
            set
            {
                if (_logFolder == null)
                {
                    _defaultLogFolder = value;
                }
                if (!string.IsNullOrEmpty(value) && (!value.StartsWith("C:\\") || Instance?.Target == MobileTarget.Windows))
                {
                    _logFolder = value;
                }
                else
                {
                    _logFolder = _defaultLogFolder;
                }
            }
        }

        public override void OnAppLoad()
        {
#if DEBUG
            Log.LoggingLevel = LoggingLevel.Debug;
#else
            Log.LoggingLevel = LoggingLevel.Error;
#endif
            Title = Factory.GetResourceString("app_name");

            var type = GetType();
            Device.Resources.AddResources(type.Namespace + ".Resources.Defaults", Device.Reflector.GetAssembly(type));

            Settings.Load();
            LogFolder = GetSetting(SettingsController.LogFolderKey);

            NavigationMap.Add("Scan", new BarcodeController());
            NavigationMap.Add("Settings", new SettingsController());

            MXContainer.AddView<ObservableCollection<string>>(new ScanView());
            MXContainer.AddView<ISettings>(typeof(SettingsView));

            var barcode = "1234567890";
            var symbol = "UPC-A";
            var localTime = DateTime.Now;
            var scanInterval = TimeSpan.FromSeconds(10);
            MXContainer.RegisterSingleton<IScanner>(typeof(MockScanner), () => new MockScanner(Convert.ToInt32(scanInterval.TotalMilliseconds), $"{barcode};{symbol};{localTime}"));

            var apiKey = GetSetting(SettingsController.APIKey);
            if (!string.IsNullOrEmpty(apiKey))
            {
                Device.RequestInjectionHeaders["apikey"] = apiKey;
            }
        }

        public override void OnControllerLoadCanceled(IMXView fromView, IMXController controller, string navigatedUri)
        {
            if (!(controller is SettingsController))
            {
                Factory.StopBlockingUserInput();
            }
        }

        public static void StartGeolocation()
        {
            var app = (App)Instance;
            if (app._geo == null)
            {
                app._geo = new GeoLocation();
                Device.Thread.ExecuteOnMainThread(app._geo.Start);
                app._geo.LocationUpdated += (o, e) =>
                {
                    _latitude = e.Data.Latitude;
                    _longitude = e.Data.Longitude;
                };
            }
        }

        public static void StopGeolocation()
        {
            var app = (App)Instance;
            if (app._geo != null)
            {
                app._geo.Stop();
                app._geo = null;
            }
        }

        public static Action<SignalType> PerformBSSIDScan { get; set; } = types => { };

        public static Action EnableGeolocation { get; set; } = () => { };

        public static List<BSSIDInfo> ScanResults { get; set; }

        public static string GetSetting(string SettingKey, bool isURI = false)
        {
            var settingString = Settings.GetValueOrDefault(SettingKey);
            if (string.IsNullOrEmpty(settingString) || isURI && !Uri.TryCreate(settingString, UriKind.Absolute, out Uri settingUri))
            {
                Settings[SettingKey] = settingString = Factory.GetResourceString("Default" + SettingKey);
                Settings.Store();
            }
            return settingString;
        }

        public static string Post(string uriKey, IPostable obj, string key, bool logResponse = false)
        {
            obj.Latitude = _latitude;
            obj.Longitude = _longitude;
            obj.Version = Factory.GetResourceString("VersionName");
            obj.Device = DeviceName;

            string dataString;
            if (obj is LocationScan data)
            {
                dataString = JsonConvert.SerializeObject(new
                {
                    scan = data.Value,
                    content = obj,
                });
            }
            else
            {
                dataString = JsonConvert.SerializeObject(new { content = obj });
            }
            var logData = BoolField.ParseValue(GetSetting(SettingsController.LogDataKey));
            if (logData)
            {
                var logFile = Path.Combine(LogFolder, $"{key}request_{DateTime.UtcNow.ToFileTimeUtc()}.log");
                try
                {
                    File.EnsureDirectoryExistsForFile(logFile);
                }
                catch (Exception e)
                {
                    Device.Log.Error(e);
                    _logFolder = _defaultLogFolder;
                }
                Device.File.Save(logFile, dataString);
            }

            var uri = GetSetting(uriKey, true);
            var responseObj = Network.Poster.PostString(uri, dataString, "application/json", Device.RequestInjectionHeaders);
            var response = responseObj?.ResponseString;
            if (!string.IsNullOrEmpty(response))
            {
                var jobject = JObject.Parse(response);
                if (jobject.ContainsKey("content"))
                {
                    response = jobject["content"].ToString();
                }
            }

            if (logData && logResponse)
            {
                Device.Thread.Start(() =>
                {
                    var responseFile = Path.Combine(LogFolder, $"{key}response_{DateTime.UtcNow.ToFileTimeUtc()}.log");
                    Device.File.Save(responseFile, response);
                });
            }

            return response;
        }
    }
}