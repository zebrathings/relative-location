﻿namespace RelativeLocation
{
    public interface IPostable
    {
        string Device { get; set; }
        string Version { get; set; }
        double Latitude { get; set; }
        double Longitude { get; set; }
    }
}