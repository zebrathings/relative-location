﻿using iFactr.Core;
using iFactr.Core.Forms;
using iFactr.UI;
using iFactr.UI.Controls;
using MonoCross;
using MonoCross.Navigation;
using MonoCross.Scanning;
using MonoCross.Utilities;
using System;
using System.ComponentModel;

namespace RelativeLocation
{
    public class ScanView : ListView<BarcodeList>
    {
        private bool _shouldUpdate;

        protected override void OnRender()
        {
            Title = iApp.Factory.GetResourceString("MainTitle");
            PreferredOrientations = PreferredOrientation.Portrait;
            Menu = new Menu(new MenuButton(iApp.Factory.GetResourceString("Settings")) { NavigationLink = new Link("Settings"), });
            _shouldUpdate = true;
            UpdateSections();

            Model.PropertyChanged -= Model_PropertyChanged;
            Model.PropertyChanged += Model_PropertyChanged;
            Activated -= ActivateScanner;
            Activated += ActivateScanner;
            Deactivated -= DeactivateScanner;
            Deactivated += DeactivateScanner;
        }

        private void ActivateScanner(object sender, EventArgs e)
        {
            var scanner = MXContainer.Resolve<IScanner>();
            if (scanner != null)
            {
                scanner.NullifyScanOccurredEvent();
                scanner.ScanOccurred += Scanner_ScanOccurred;
                scanner.StartScan();
            }
        }

        private void DeactivateScanner(object sender, EventArgs e)
        {
            var scanner = MXContainer.Resolve<IScanner>();
            if (scanner != null)
            {
                scanner.StopScan();
                scanner.NullifyScanOccurredEvent();
            }
        }

        private void Scanner_ScanOccurred(string scannedValue)
        {
            Device.Thread.ExecuteOnMainThread(() =>
            {
                iApp.Factory.ActivateLoadTimer();
                DeactivateScanner(null, EventArgs.Empty);
            });
            Model.SubmitBarcode(scannedValue, () =>
            {
                iApp.Factory.StopBlockingUserInput();
                ActivateScanner(null, EventArgs.Empty);
            });
        }

        protected override bool ShouldNavigateFrom(Link link, NavigationType type)
        {
            if (iApp.Instance.NavigationMap.MatchUrl(link.Address).Controller is BarcodeController)
            {
                ActivateScanner(this, EventArgs.Empty);
            }
            else
            {
                DeactivateScanner(this, EventArgs.Empty);
            }
            return base.ShouldNavigateFrom(link, type);
        }

        protected override int OnItemIdRequested(int section, int index) { return section == 0 ? index + 1 : 0; }

        protected override ICell OnCellRequested(int section, int index, ICell recycledCell)
        {
            if (section == 0)
            {
                switch (index)
                {
                    default:
                        var valueCell = recycledCell as GridCell ?? new GridCell();
                        valueCell.SetRows(1);
                        valueCell.SetColumns(2);

                        var symbol = valueCell.GetOrCreateChild<Label>("symbol");
                        symbol.RowIndex = 0;
                        symbol.ColumnIndex = 0;
                        symbol.Font = new Font(symbol.Font.Name, symbol.Font.Size, FontFormatting.Bold);
                        symbol.Margin = new Thickness(0, 0, Thickness.SmallHorizontalSpacing, 0);

                        var value = valueCell.GetOrCreateChild<Label>("value");
                        value.RowIndex = 0;
                        value.ColumnIndex = 1;

                        if (Model.Barcode == null)
                        {
                            symbol.Visibility = Visibility.Collapsed;
                            value.Text = iApp.Factory.GetResourceString("ScanHelp");
                        }
                        else
                        {
                            symbol.Visibility = Visibility.Visible;
                            symbol.Text = Model.Barcode.Symbology + ":";
                            value.Text = Model.Barcode.Value;
                        }
                        return valueCell;
                    case 1:
                        if (Model.Barcode == null)
                        {
                            return GetCameraButton();
                        }
                        var cell = new MultiLineTextField("Description")
                        {
                            Label = iApp.Factory.GetResourceString("Description"),
                        }.Convert(iApp.Instance.Style, this, null);
                        var textbox = ((IElementHost)cell).GetChild<ITextArea>();
                        textbox.GotFocus += (o, e) =>
                        {
                            _shouldUpdate = false;
                            DeactivateScanner(this, EventArgs.Empty);
                        };
                        textbox.LostFocus += (o, e) =>
                        {
                            if (string.IsNullOrEmpty(((ITextArea)o).Text))
                            {
                                _shouldUpdate = true;
                                UpdateSections();
                                ActivateScanner(this, EventArgs.Empty);
                            }
                        };
                        return cell;
                    case 2:
                        var buttonCell = new ButtonField(null, ".")
                        {
                            Label = iApp.Factory.GetResourceString("Submit"),
                        }.Convert(iApp.Instance.Style, this, null);
                        var button = ((IElementHost)buttonCell).GetChild<IButton>();
                        button.Clicked += (o, e) =>
                        {
                            _shouldUpdate = true;
                            Submit(".");
                        };
                        return buttonCell;
                    case 3:
                        return GetCameraButton();
                }
            }
            else
            {
                var data = Model[index];
                var cell = recycledCell as ContentCell ?? new ContentCell();
                cell.TextLabel.Text = data.Barcode_Value;
                cell.ValueLabel.Text = $"{data.Confidence / 100:P2}";
                return cell;
            }
        }

        private ICell GetCameraButton()
        {
            var scanner = MXContainer.Resolve<IScanner>();
            var buttonCell = new ButtonField(null, null)
            {
                Label = iApp.Factory.GetResourceString("CameraScan"),
            }.Convert(iApp.Instance.Style, this, null);
            var button = ((IElementHost)buttonCell).GetChild<IButton>();
            button.Clicked += (o, e) =>
            {
                scanner.StartScan(true);
            };
            return buttonCell;
        }

        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (_shouldUpdate)
            {
                UpdateSections();
                ReloadSections();
            }
        }

        private void UpdateSections()
        {
            int baseCount = 1;
            if (iApp.Factory.Platform == MobilePlatform.Android && MXContainer.Resolve<IScanner>().GetType().Name.StartsWith("Camera"))
            {
                baseCount++;
            }
            Sections[0].ItemCount = Model.Barcode == null ? baseCount : baseCount + 2;
            Sections[1].ItemCount = Model.Count;
        }
    }
}