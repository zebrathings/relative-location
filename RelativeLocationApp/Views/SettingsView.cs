﻿using iFactr.Core;
using iFactr.Core.Forms;
using iFactr.Core.Targets.Settings;
using iFactr.UI;
using iFactr.UI.Controls;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RelativeLocation
{
    public class SettingsView : ListView<ISettings>
    {
        private List<Field> _fields = new List<Field>();
        protected override void OnRender()
        {
            Title = iApp.Factory.GetResourceString("Settings");
            _fields.Clear();
            _fields.Add(new BoolField(SettingsController.SignalTypeKey)
            {
                Label = iApp.Factory.GetResourceString(SettingsController.SignalTypeKey),
                Value = BoolField.ParseValue(App.GetSetting(SettingsController.SignalTypeKey)),
            });
            _fields.Add(new BoolField(SettingsController.GPSKey)
            {
                Label = iApp.Factory.GetResourceString(SettingsController.GPSKey),
                Value = BoolField.ParseValue(App.GetSetting(SettingsController.GPSKey)),
            });
            _fields.Add(new BoolField(SettingsController.LogDataKey)
            {
                Label = iApp.Factory.GetResourceString(SettingsController.LogDataKey),
                Value = BoolField.ParseValue(App.GetSetting(SettingsController.LogDataKey)),
            });
            _fields.Add(new TextField(SettingsController.LogFolderKey)
            {
                Label = iApp.Factory.GetResourceString(SettingsController.LogFolderKey),
                Text = App.LogFolder,
            });
            _fields.Add(new NumericField(SettingsController.ScanIntervalKey)
            {
                Label = iApp.Factory.GetResourceString(SettingsController.ScanIntervalKey),
                Text = App.GetSetting(SettingsController.ScanIntervalKey),
                Placeholder = iApp.Factory.GetResourceString($"Default{SettingsController.ScanIntervalKey}"),
                KeyboardType = iFactr.Core.Forms.KeyboardType.PIN,
            });
            _fields.Add(new BoolField(SettingsController.TrainingModeKey)
            {
                Label = iApp.Factory.GetResourceString(SettingsController.TrainingModeKey),
                Value = BoolField.ParseValue(App.GetSetting(SettingsController.TrainingModeKey)),
            });
            _fields.Add(new TextField(SettingsController.BarcodeURIKey)
            {
                Label = iApp.Factory.GetResourceString(SettingsController.BarcodeURIKey),
                Text = App.GetSetting(SettingsController.BarcodeURIKey, true),
                Placeholder = iApp.Factory.GetResourceString($"Default{SettingsController.BarcodeURIKey}"),
            });
            _fields.Add(new TextField(SettingsController.BarcodeTrainingURIKey)
            {
                Label = iApp.Factory.GetResourceString(SettingsController.BarcodeTrainingURIKey),
                Text = App.GetSetting(SettingsController.BarcodeTrainingURIKey, true),
                Placeholder = iApp.Factory.GetResourceString($"Default{SettingsController.BarcodeTrainingURIKey}"),
            });
            _fields.Add(new TextField(SettingsController.BssidURIKey)
            {
                Label = iApp.Factory.GetResourceString(SettingsController.BssidURIKey),
                Text = App.GetSetting(SettingsController.BssidURIKey, true),
                Placeholder = iApp.Factory.GetResourceString($"Default{SettingsController.BssidURIKey}"),
            });
            _fields.Add(new TextField(SettingsController.BarcodeInfoURIKey)
            {
                Label = iApp.Factory.GetResourceString(SettingsController.BarcodeInfoURIKey),
                Text = App.GetSetting(SettingsController.BarcodeInfoURIKey, true),
                Placeholder = iApp.Factory.GetResourceString($"Default{SettingsController.BarcodeInfoURIKey}"),
            });
            _fields.Add(new TextField(SettingsController.APIKey)
            {
                Label = iApp.Factory.GetResourceString(SettingsController.APIKey),
                Text = App.GetSetting(SettingsController.APIKey),
            });
            _fields.Add(new LabelField()
            {
                Label = iApp.Factory.GetResourceString("VersionLabel"),
                Text = iApp.Factory.GetResourceString("VersionName"),
            });
            Sections[0].ItemCount = _fields.Count;
            var submitButton = new MenuButton(iApp.Factory.GetResourceString("Submit"));
            submitButton.Clicked += Submission;
            Menu = new Menu(submitButton);
        }

        protected override ICell OnCellRequested(int section, int index, ICell recycledCell)
        {
            var cell = _fields[index].Convert(iApp.Instance.Style, this, recycledCell);
            cell.Metadata["Index"] = index;
            var textbox = ((IElementHost)cell).GetChild<ITextBox>();
            if (textbox != null)
            {
                if (textbox.SubmitKey == SettingsController.LogFolderKey)
                {
                    textbox.TextChanged -= UpdateBool;
                    textbox.TextChanged += UpdateBool;
                }

                if (textbox.SubmitKey == _fields.OfType<TextField>().LastOrDefault().ID)
                {
                    textbox.KeyboardReturnType = KeyboardReturnType.Go;
                    textbox.ReturnKeyPressed -= Submission;
                    textbox.ReturnKeyPressed += Submission;
                }
                else
                {
                    textbox.KeyboardReturnType = KeyboardReturnType.Next;
                    textbox.ReturnKeyPressed -= NextTextbox;
                    textbox.ReturnKeyPressed += NextTextbox;
                }
            }
            return cell;
        }

        protected override int OnItemIdRequested(int section, int index)
        {
            return index;
        }

        private void UpdateBool(object sender, ValueChangedEventArgs<string> args)
        {
            var index = (int)((ICell)((ITextBox)sender).Parent).Metadata["Index"];
            foreach (ICell c in GetVisibleCells())
            {
                if ((int)c.Metadata["Index"] == index - 1)
                {
                    ((IElementHost)c).GetChild<ISwitch>().Value = true;
                    break;
                }
            }
        }

        private void NextTextbox(object sender, EventHandledEventArgs e)
        {
            var index = (int)((ICell)((ITextBox)sender).Parent).Metadata["Index"];
            var visibleCells = GetVisibleCells().ToArray();
            for (int i = 0; i < visibleCells.Length; i++)
            {
                var cellIndex = (int)visibleCells[i].Metadata["Index"];
                if (cellIndex > index)
                {
                    if (i == visibleCells.Length - 1)
                    {
                        ScrollToCell(0, cellIndex, true);
                    }
                    var t = ((IElementHost)visibleCells[i]).GetChild<ITextBox>();
                    if (t != null)
                    {
                        t.Focus();
                        break;
                    }
                }
            }
        }

        private void Submission(object sender, EventArgs e)
        {
            Submit(".");
        }

        protected override bool ShouldNavigateFrom(Link link, NavigationType type)
        {
            if (type == NavigationType.Back)
            {
                Submission(this, EventArgs.Empty);
                return false;
            }
            else
            {
                return base.ShouldNavigateFrom(link, type);
            }
        }
    }
}