﻿using System.Collections.Generic;

namespace RelativeLocation
{
    public class LocationScan : Barcode
    {
        public List<BSSIDInfo> ScanResults { get; set; }
    }
}