﻿using System.Collections.Generic;

namespace RelativeLocation
{
    public class BSSIDList : IPostable
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Version { get; set; }
        public string Device { get; set; }
        public List<BSSIDInfo> ScanResults { get; set; }
    }
}