﻿using System;

namespace RelativeLocation
{
    [Flags]
    public enum SignalType
    {
        Wifi = 1,
        Bluetooth = 2,
    }
}