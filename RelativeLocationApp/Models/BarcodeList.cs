﻿using iFactr.Core.Forms;
using MonoCross.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Timers;

namespace RelativeLocation
{
    public class BarcodeList : List<BarcodeConfidence>, INotifyPropertyChanged
    {
        private Timer _timer;
        public double ScanInterval
        {
            get => _timer.Interval;
            set
            {
                if (_timer.Interval != value)
                {
                    _timer.Interval = value;
                }
            }
        }

        public BarcodeList()
        {
            _timer = new Timer()
            {
                AutoReset = true,
                Interval = int.Parse(App.GetSetting(SettingsController.ScanIntervalKey)) * 1000,
            };
            _timer.Elapsed += _timer_Elapsed;
            Device.Thread.Start(() =>
            {
                _timer_Elapsed(_timer, EventArgs.Empty);
                _timer.Start();
            });
        }

        public Barcode Barcode
        {
            get => _barcode;
            set
            {
                _barcode = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Barcode)));
            }
        }
        private Barcode _barcode;

        public event PropertyChangedEventHandler PropertyChanged;

        private void _timer_Elapsed(object sender, EventArgs args)
        {
            if (App.ScanResults != null)
            {
                var logData = BoolField.ParseValue(App.GetSetting(SettingsController.LogDataKey));
                var rssi = new BSSIDList
                {
                    ScanResults = new List<BSSIDInfo>(App.ScanResults),
                };
                App.Post(SettingsController.BssidURIKey, rssi, "bssid", true);
            }
            App.ScanResults = new List<BSSIDInfo>();
            App.PerformBSSIDScan(BoolField.ParseValue(App.GetSetting(SettingsController.SignalTypeKey)) ? SignalType.Bluetooth | SignalType.Wifi : SignalType.Wifi);
        }


        public void SubmitBarcode(string scannedValue, Action callback)
        {
            Device.Thread.Start(() =>
            {
                var value = scannedValue.Split(';');
                if (value.Length < 3 || !DateTime.TryParse(value[2], out DateTime localScanTime))
                {
                    throw new FormatException("Scan string must be in the format of <barcode>;<symbology>;<localTime>");
                }
                var data = new LocationScan
                {
                    Value = value[0],
                    Symbology = value[1],
                    BarcodeScanUtc = localScanTime.ToUniversalTime(),
                    ScanResults = new List<BSSIDInfo>(App.ScanResults),
                };

                var uriKey = BoolField.ParseValue(App.GetSetting(SettingsController.TrainingModeKey)) ? SettingsController.BarcodeTrainingURIKey : SettingsController.BarcodeURIKey;
                var response = App.Post(uriKey, data, "barcode", true);
                if (!string.IsNullOrEmpty(response))
                {
                    Clear();
                    var responseObj = JsonConvert.DeserializeObject<ConfidenceCollection>(response);
                    AddRange(responseObj?.Location?.OrderByDescending(c => c.Confidence) ?? Enumerable.Empty<BarcodeConfidence>());
                }
                Barcode = data;
                if (callback != null)
                {
                    Device.Thread.ExecuteOnMainThread(callback);
                }
            });
        }
    }
}