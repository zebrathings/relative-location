﻿using System;

namespace RelativeLocation
{
    public class Barcode : IPostable
    {
        public DateTime BarcodeScanUtc { get; set; }
        public string Value { get; set; }
        public string Symbology { get; set; }
        public string Device { get; set; }
        public string Version { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}