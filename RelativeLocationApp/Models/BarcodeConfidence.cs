﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RelativeLocation
{
    public class BarcodeConfidence
    {
        public double Confidence { get; set; }
        public string Barcode_Value { get; set; }
    }
}
