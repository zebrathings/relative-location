﻿using System;

namespace RelativeLocation
{
    public class BSSIDInfo
    {
        public string BSSID { get; set; }
        public DateTime RecUtc { get; set; }
        public double SignalStrength { get; set; }
        public SignalType SignalType { get; set; }
    }
}